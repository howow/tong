'use strict'

import got from 'got'

import {
  __, T, F, I, o,
  compose, objOf, curryN, otherwise, andThen, propEq, path, cond, always,
  memoizeWith, useWith, mergeRight,
  // log, llog, xlog, xllog, print, list,
  ignoreError, envKey
} from 'robin'

/**
 *  == geo ==
 */
const authKey = envKey('GOOGLE_GEOCODE_KEY')

const setParam = compose(
  objOf('searchParams'),
  useWith(mergeRight, [objOf('key'), objOf('address')])
)

const geo = curryN(2, got.extend({
  prefixUrl: 'https://maps.googleapis.com/maps/api/geocode/',
  searchParams: {
    language: 'zh-TW'
  },
  responseType: 'json',
  method: 'GET',
  resolveBodyOnly: true,
  timeout: {
    request: 10000
  }
}))

const emptyLatLng = always({ lat: 0, lng: 0 })

const lookupLatLng = cond([
  [propEq('status', 'OK'), path(['results', 0, 'geometry', 'location'])],
  [T, emptyLatLng]
])

const initAPI = compose(
  otherwise(o(emptyLatLng, ignoreError)),
  andThen(lookupLatLng),
  geo('json'),
  setParam
)

const mapLatLng = memoizeWith(I, (address) => {
  const key = authKey(process.env)
  return initAPI(key, address)
})

export {
  mapLatLng
}
