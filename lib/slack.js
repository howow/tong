'use strict'

import got from 'got'

import {
  __, T, F, I, o,
  curryN, otherwise, andThen, set, lensPath, compose,
  // log, llog, xlog, xllog, print,
  ignoreError, envKey
} from 'robin'

/**
 *  == slack ==
 */
const apiPath = envKey('SLACK_MESSAGE_API')

const slack = curryN(2, got.extend({
  prefixUrl: 'https://hooks.slack.com/services/',
  //  responseType: 'json',
  method: 'POST',
  resolveBodyOnly: true,
  timeout: {
    request: 10000
  }
}))

const setTextMessage = set(lensPath(['json', 'text']), __, {})

const initAPI = compose(
  otherwise(ignoreError),
  andThen(T),
  slack
)

const warning = curryN(2, (title, text) => {
  const api = apiPath(process.env)
  const context = `[${title}] ${text}`

  return initAPI(api, setTextMessage(context))
})

export {
  warning
}
