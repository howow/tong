'use strict'

import {
  o, always, compose, curryN, once, bind,
  mergeDeepRight, converge, mergeRight, objOf, ifElse, last, prop,
  envConfig, ignoreError, hasValue, alwaysEmptyObject
} from 'robin'

import { Storage } from '@google-cloud/storage'

import mime from 'mime'

import { extname } from 'path'

const getType = bind(mime.getType, mime)

const storage = once(() => {
  const config = envConfig(/^GOOGLE_STORAGE/, process.env)
  return new Storage(config)
})

const defaultHeader = {
  public: true,
  metadata: {
    cacheControl: 'public, no-cache'
  }
}

const mimeType = compose(
  ifElse(hasValue, objOf('contentType'), alwaysEmptyObject),
  getType,
  extname
)

const metadata = compose(
  mergeDeepRight(defaultHeader),
  converge(mergeRight, [objOf('destination'), o(objOf('metadata'), mimeType)])
)
/**
 * 上傳到 google storage
 *
 * @param {String} src 本地檔名路徑
 * @param {String} dest 遠端檔名
 * @returns response
 */
const uploadToBucket = curryN(3, (bucketName, src, dest) => {
  const bucket = storage().bucket(bucketName)
  return bucket.upload(src, metadata(dest))
    .catch(o(always([{}]), ignoreError))
    .then(o(prop('name'), last))
})

export {
  uploadToBucket
}
